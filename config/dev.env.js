var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_URL:  '"http://zeus.local/"',
  VERSION: '"1.0.0"',
})
