import http from '../../http'

// export default {
//   user: {
//     authenticated: false
//   },
//   // realizar o login do usuario do sistema
  // login (context, creds) {
  //   http.post('http://zeus.local/autenticate', { grant_type: 'password', client_id: 'zeusmaster', client_secret: '123deoliveira4', username: 'zeusmaster', password: '123deoliveira4' })
  //     .then(response => response.data)
  //     .then(data => {
  //       localStorage.setItem('token', data.access_token)
  //       this.user.authenticated = true
  //     })
//   },
//   // Realizar o logout da area do sistema
//   logout () {
//     localStorage.removeItem('token')
//     this.user.authenticated = false
//   },
//   // recupara o Token do localStorage para uso na aplicacao
//   getAuthHeader () {
//     return {
//       'access': '' + localStorage.getItem('token')
//     }
//   },
//   checkAuth () {
//     let validate = localStorage.getItem('token')
//     if (validate) {
//       this.user.authenticated = true
//     } else {
//       this.user.authenticated = false
//     }
//   }
// }
export const postLogin = (user) => {
  return http.post(process.env.API_URL + 'autenticate', user)
    .then(response => response.data)
}
