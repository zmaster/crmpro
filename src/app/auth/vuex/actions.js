import * as types from './mutations-types'
import { postLogin } from '../login'

// export const attemptLogin = (context, payload) => {
//   console.log('TESTE')
//   // context.commit('setToken', 'xxx')
//   // return http.post('http://zeus.local/autenticate', { grant_type: 'password', client_id: 'zeusmaster', client_secret: '123deoliveira4', username: 'zeusmaster', password: '123deoliveira4' })
//   //   .then(response => response.data)
//   //   .then(data => {
//   //     context.commit(types.setToken, data.access_token)
//   //     context.commit(types.setUser, data)
//   //   })
// }

export const attemptLogin = (context, payload) => {
  return postLogin(payload)
    .then(data => {
      localStorage.setItem('token', data.access_token)
      let token = localStorage.getItem('token')
      context.commit(types.setToken, token)
      context.commit(types.setUser, data)
    })
}
