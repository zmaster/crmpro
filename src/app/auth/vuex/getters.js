import { isEmpty } from 'lodash'
// import auth from '../login'
export const isLogged = ({ token }) => !isEmpty(token)
export const currentUser = ({ user }) => user
