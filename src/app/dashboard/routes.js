import LayoutBase from '../../components/layout/Layout.vue'
import Main from './components/Main'

export default [
  {
    path: '/',
    component: LayoutBase,
    children: [
      { path: '', component: Main, name: 'dashboard.main' }
    ]
  }
]
