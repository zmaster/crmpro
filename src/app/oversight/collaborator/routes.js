import LayoutBase from '../../../components/layout/Layout.vue'
import Main from './components/Main'
import CreateCollaborator from './components/Create'
import EditCollaborator from './components/Edit'
export default [
  {
    path: '/collaborator',
    component: LayoutBase,
    children: [
      { path: '', component: Main, name: 'collaborator.main' },
      { path: 'create', component: CreateCollaborator, name: 'collaborator.create' },
      { path: 'edit/:id/plan/:idPlan', component: EditCollaborator, name: 'collaborator.edit' }
    ]
  }
]
