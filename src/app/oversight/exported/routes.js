import LayoutBase from '../../../components/layout/Layout.vue'
import Exported from './components/Create'

export default [
  {
    path: '/export',
    component: LayoutBase,
    children: [
      { path: '', component: Exported, name: 'exported.create' }
    ]
  }
]
