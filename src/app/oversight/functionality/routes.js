import LayoutBase from '../../../components/layout/Layout.vue'
import Main from './components/Main'
import CreateFuncionalidade from './components/Create'
import EditFuncionalidade from './components/Edit'

export default [
  {
    path: '/funcionalidade',
    component: LayoutBase,
    children: [
      { path: '', component: Main, name: 'funcionalidade.main' },
      { path: '/funcionalidade/create', component: CreateFuncionalidade, name: 'funcionalidade.create' },
      { path: '/funcionalidade/edit/:id', component: EditFuncionalidade, name: 'funcionalidade.edit' }
    ]
  }
]
