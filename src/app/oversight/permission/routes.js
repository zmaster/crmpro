import LayoutBase from '../../../components/layout/Layout.vue'
import Main from './components/Main'
import CreatePermission from './components/Create'
import EditPermission from './components/Edit'
export default [
  {
    path: '/permissao',
    component: LayoutBase,
    children: [
      { path: '', component: Main, name: 'permission.main' },
      { path: '/permissao/create', component: CreatePermission, name: 'permission.create' },
      { path: '/permissao/edit/:id', component: EditPermission, name: 'permission.edit' }
    ]
  }
]
