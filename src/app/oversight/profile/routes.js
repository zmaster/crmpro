import LayoutBase from '../../../components/layout/Layout.vue'
import Main from './components/Main'
import CreateProfile from './components/Create'
import EditProfile from './components/Edit'
export default [
  {
    path: '/perfil',
    component: LayoutBase,
    children: [
      { path: '', component: Main, name: 'profile.main' },
      { path: '/perfil/create', component: CreateProfile, name: 'profile.create' },
      { path: '/perfil/edit/:id', component: EditProfile, name: 'profile.edit' }
    ]
  }
]
