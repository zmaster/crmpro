import LayoutBase from '../../../components/layout/Layout.vue'
import Main from './components/Main'
import CreateCompany from './components/Create'
import EditCompany from './components/Edit'
import ContactHistoryCompany from './components/ContactHistory'

export default [
  {
    path: '/company',
    component: LayoutBase,
    children: [
      { path: '', component: Main, name: 'company.main' },
      { path: 'create', component: CreateCompany, name: 'company.create' },
      { path: 'edit/:id', component: EditCompany, name: 'company.edit' },
      { path: 'contacthistory/:id', component: ContactHistoryCompany, name: 'company.contacthistory' }
    ]
  }
]
