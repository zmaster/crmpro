import LayoutBase from '../../../components/layout/Layout.vue'
import Main from './components/Main'
import CreatePerson from './components/Create'
import EditPerson from './components/Edit'
import PersonHistory from './components/PersonHistory'

export default [
  {
    path: '/pessoa',
    component: LayoutBase,
    children: [
      { path: '', component: Main, name: 'person.main' },
      { path: 'create', component: CreatePerson, name: 'person.create' },
      { path: 'edit/:id', component: EditPerson, name: 'person.edit' },
      { path: 'personhistory/:id', component: PersonHistory, name: 'person.personhistory' }
    ]
  }
]
