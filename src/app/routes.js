import { routes as collaborator } from './oversight/collaborator'
import { routes as profile } from './oversight/profile'
import { routes as funcionalidade } from './oversight/functionality'
import { routes as permissao } from './oversight/permission'
import { routes as auth } from './auth'
import { routes as company } from './relationships/company'
import { routes as person } from './relationships/person'
import { routes as exported } from './oversight/exported'
import { routes as dashboard } from './dashboard'

export default [
  ...collaborator, ...profile, ...funcionalidade, ...permissao, ...auth,
  ...company, ...person, ...exported, ...dashboard
]
