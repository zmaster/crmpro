import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import '../src/assets/css/crmpro.css'
import VueResource from 'vue-resource'
import router from './router'
import store from './vuex'
import VueMask from 'di-vue-mask'
import quillEditor from 'vue-quill-editor'
import VeeValidate, { Validator } from 'vee-validate'
import messages from 'vee-validate/dist/locale/pt_BR'
import Toasted from 'vue-toasted'

const moment = require('moment')
require('moment/locale/br')

Vue.use(Vuetify)
Vue.use(VueResource)
/** Activate vue.js plugins **/
Vue.use(VueMask)
Vue.use(require('vue-moment'), {
  moment
})

Vue.use(Toasted)
Vue.use(quillEditor)
// Merge the locales.
Validator.addLocale(messages)
// Config for Validator
const config = {
  errorBagName: 'errors', // change if property conflicts.
  fieldsBagName: 'fields',
  delay: 0,
  locale: 'pt_BR',
  dictionary: null,
  strict: true,
  enableAutoClasses: false,
  classNames: {
    touched: 'touched', // the control has been blurred
    untouched: 'untouched', // the control hasn't been blurred
    valid: 'valid', // model is valid
    invalid: 'invalid', // model is invalid
    pristine: 'pristine', // control has not been interacted with
    dirty: 'dirty' // control has been interacted with
  },
  events: 'input|blur',
  inject: false
}
Vue.use(VeeValidate, config)
Vue.config.productionTip = false

Vue.http.options.emulateJSON = true
Vue.http.options.emulateHTTP = true
Vue.config.debug = true
Vue.http.interceptors.push((request, next) => {
  request.headers.set('Authorization', 'Bearer ' + localStorage.getItem('token'))
  request.headers.set('Accept', 'application/json')
  next((response) => {
    if (response.status === 401) {
      localStorage.removeItem('token')
      window.location.href = '/auth'
    }
    if (response.status === 403) {
      router.go(-1)
    }
  })
})

/* eslint-disable no-new */
new Vue({
  router,
  store,
  el: '#app',
  render: h => h(App)
})
