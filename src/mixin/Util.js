var Util = {
  methods: {
    justNumber (strParam) {
      if (strParam) {
        return strParam.replace(/[^\d]/g, '')
      }
      return null
    },
    dateFormatPost (strParam) {
      if (strParam) {
        let date = strParam.split('/')
        return date[2] + '-' + date[1] + '-' + date[0]
      }
      return null
    },
    dateFormatError (strParam) {
      if (strParam) {
        return strParam.substring(6, 8) + strParam.substring(4, 6) + strParam.substring(0, 4)
      }
      return null
    }
  }
}

export default Util
