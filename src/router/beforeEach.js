// import store from '../vuex'
// import auth from '../app/auth/login'
const isAuthRoute = route => route.path.indexOf('/auth') !== -1
const isLogged = () => localStorage.getItem('token')

export default (to, from, next) => {
  if (!isAuthRoute(to) && !isLogged()) {
    next('/auth')
  } else {
    next()
  }
}
