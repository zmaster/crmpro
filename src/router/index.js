import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'
import beforeEach from './beforeEach'

Vue.use(VueRouter)

const router = new VueRouter({
  routes,
  hashbang: false,
  history: true,
  transitionOnLoad: true,
  // mode: 'html5'
  mode: 'history'
})
router.mode = 'html5'

router.beforeEach(beforeEach)

export default router
