import { routes as app } from '../app'

const root = [
 { path: '/', redirect: {name: 'dashboard.main'} }
]

export default [ ...root, ...app ]
